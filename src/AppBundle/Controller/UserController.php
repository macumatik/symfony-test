<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



class UserController extends Controller
{
    /**
     * @Route("/", name="user_index")
     */
    public function indexAction()
    {
        $ac = $this->get('security.authorization_checker');

        if ($ac->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute("user_list");
        } else {
            return $this->redirectToRoute("login_login");
        }
    }

    /**
     * @Route("/register", name="user_add")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function addAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $em->persist($user);
            $em->flush();

            $this->addFlash('notice', 'Użytkownik dodany');
            return $this->redirectToRoute("user_index");
        }

        return $this->render("User/add.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @Route("/panel/list", name="user_list")
     * @return Response
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();

        return $this->render("User/list.html.twig", ['subtxt' => 'test', 'users' => $users]);
    }

    /**
     * @Route("/panel/edit/{id}", name="user_edit")
     * @param User $user
     * @return JsonResponse
     */
    public function editAction(User $user)
    {
        $userArray = [
            'username' => $user->getUsername(),
            'email' => $user->getEmail()
        ];
        return $this->json($userArray);
    }

    /**
     * @Route("/panel/insert", name="user_insert")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function insertAction(Request $request, UserPasswordEncoderInterface $encoder)
    {

        if ($request->isMethod('POST')) {
            $username = $request->get('username');
            $email = $request->get('email');
            $password = $request->get('email');

            $user = new User();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($encoder->encodePassword($user, $password));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->json(['info' => $username, 'id' => $user->getId()]);
        } else {
            return false;
        }
    }

    /**
     * @Route("/panel/update", name="user_update")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateAction(Request $request)
    {
        if ($request->isMethod('PATCH')) {
            $id = $request->get('id');
            $username = $request->get('username');
            $email = $request->get('email');

            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository(User::class)->find($id);
            $user->setUsername($username);
            $user->setEmail($email);

            $em->persist($user);
            $em->flush();

            return $this->json(['info' => 'ok']);
        } else {
            return false;
        }

    }

    /**
     * @Route("/panel/user_delete", name="user_delete")
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        if ($request->isMethod('DELETE')) {
            $id = $request->get('id');
            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository(User::class)->find($id);
            $em->remove($user);
            $em->flush();

            return $this->json(['info' => 'ok']);
        } else {
            return false;
        }

    }
}